#LectureDoc Main Documentation   {title:LectureDocDocumentation}

Note: Throughout this document, LectureDoc is abbreviated as LD.
- - - -

##Overall architecture

LD implements the MVC pattern as shown in the following figure.

![MVC](mvc.svg)

A typical update cycle works as follows:


1. The user sends a signal to a UI component (e.g. presses a button).
2. Each UI component is associated with an Action.
  This signal triggers the ``execute`` function associated with this action.
3. This function may have several ``update`` calls.
4. Each ``update`` comes with a list of key-value pairs to override the internal state.
5. This list is validated, filtered and then merged into the current state.
6. The state notifies listeners that are interested in the changes applied to the state.
  Actions that are registered as listeners suppress notifications that do not affect the respective Actions.
7. Each listener may return a list of key-value pairs to change the state again.
  These lists are merged to a single list of new (pending) updates.
8. When all listeners have been notified about the applied changes to the state, the pending updates are applied for another update pass.
  This loop is repeated until no listener requests any changes to the state.

- - - -

##Using the State object

###Overview and persistency

The State is an object that encapsulates variables necessary for LD to communicate with its environment, especially the UI and the web storage.
This set of variables can semantically be split into two parts called ``sessionState`` and ``applicationState``.
The session state is volatile: it is lost every time the web browser discards the variables used in the script. (This usually happens if the web browser or its window is closed, for example.)
Furthermore, the current implementation uses the session state as a cache for miscellaneous data.

The application state is persistently stored using the web storage.
That is, LD saves the application state across multiple web browser sessions. 
The variables which belong to the application state are:
* ``mode``
* ``currentSlide``
* ``currentAnimationStep``
* ``doNotShowMenuHint``
* ``lightTableZoomFactor``
* ``presentationZoomFactor``
* ``isEnhancedPresentationModeActive``
* ``clientNotes``
* ``widgets``

Note that the state object itself does not make use of the web storage and, as a consequence, does not store any variables persistently.
In order to actually store the application state persistently, a method called ``State.getApplicationState`` is provided to create a copy of the current application state.
LD uses this copy to store the application state in the web storage.

Apart from their different volatility, both the session state and the application state work the same way.
That is, the programmer that uses the state object usually does not need to distinguish between these two disjoint subsets.

###Reading

The current state can be read using the ``State.get`` function or the ``State.getApplicationState`` function.
``State.get(X)`` returns the value of the variable identified by X.
``State.getApplicationState()`` returns a copy of the application state as a Javascript object with the keys representing the identifiers of the variables and the values representing their respective values.
Modifying this object does not affect the application state.
Reading an unknown variable returns ``undefined``.

###Writing

The state can be modified using the ``State.update`` method.

If ``State.update`` is called, all state variables are overridden by their respective new values as determined by the object passed to the ``update`` function.
Variables that are not used in this object are not changed.
If a state variable is set to the same value it already has (according to ``===``), it is not changed.
If the modification of a variable does not pass the test (that is, the ``validate`` function returns ``false``), this variable is not changed.
Note that this might lead to an inconsistent state.
See [this section](#debugging) to detect such cases.
If a variable is not found in the application state, it is assumed that it belongs to the session state.
That is, new variables are always put into the session state.

For the callers of ``State`` functions, all variables that have changed are set to their new values at once (atomically).

Thereafter, these changes are propagated to the listeners.
Only changes are considered when calculating which listeners will be notified.

Calling ``update`` while events are dispatched is deprecated as this might lead to an invalid state.
See [this section](#debugging) to detect such cases.
Instead, the listener should return an object that reflects the requested changes the same way it would pass them to the ``update`` function.
Those pending updates are not visible to other listeners.
Instead, those pending updates are collected and then merged to one big update.
If this merged update set is empty, the update loop terminates.
Otherwise, the update process is repeated in a new update pass for this merged update set.

However, if two listeners request conflicting changes (i.e. the requested new values for a single variable are not equal according to ``===``), the conflicting changes requested by the first listener will be overridden by the corresponding changes requested by the second listener.
This might also lead to an invalid state.
Again, see [this section](#debugging) to detect such cases.


Calling ``State.update({})`` does not have any effect.


###Validation

There is a function called ``State.addConstraint`` which can be used to install restrictions to state variables.
Validation takes place during an update pass before state listeners are notified about changes.
Each variable is validated individually and only if the value of the respective variable is about to be changed (according to ``===``).

Currently, it is not necessary (and thus, not implemented) to validate state changes as a whole (i.e. across multiple variable changes).


###Installing listeners and how events are dispatched

A functionality that is interested in changes to the state (e.g. Actions) can register listeners (observers).
A listener can be registered using the ``State.addListener`` method in one of two different ways with slightly different semantics:

* Along with a set of state variable identifiers. This set must be specified as an array, e.g. ``State.addListener(aFunction, ["anIdentifier", "anotherIdentifier"]);``
  A listener registered this way is notified (that is, ``aFunction`` is called) every time ``State.update`` is called and at least one of the variables specified has changed since the last update, if any.
  If the listener is interested in only one variable, the identifier of this variable can be specified directly as a string (i.e. no array is necessary in this case).

* Without any set of state variable identifiers, e.g.

    ``State.addListener(aFunction);``

  A listener registered this way is notified (that is, ``aFunction`` is called) every time ``State.update()`` is called and there is at least one variable that has been changed since the last update, if any.

All listeners registered to the state are notified only once per update regardless of how many variables they are interested in.
The current implementation guarantees a partial order in which listeners are notified:
If a listener ``A`` is registered before a listener ``B``, then ``A`` will be notified before ``B``.

###Exchange with other LectureDoc Instances

The state can be updated from outside of LectureDoc via calls to ``LectureDoc.updateState()`` which uses ``State.getExchangeState`` to determine
which variables to exclude. For additional information exchange see [this section](#presentationwindow)

The variables that are currently excluded are:
* ``presentationZoomFactor``
* ``mode``
* ``isEnhancedPresentationModeActive``
* ``isMenubarVisible``
* ``isMarionette``
* ``isModalDialogActive``
* ``doNotShowMenuHint``

###Thread-safety

The current implementation of the MVC pattern assumes that Javascript is single-threaded.
That is, thread safety precautions are neither necessary nor implemented.

- - - -

##Using Actions

###Overview

An Action encapsulates

* an ID,
* the actual function which is called when the action is triggered, and
* all features that are necessary to make this function available to the UI user (e.g., labels, icons, keystrokes).

These features are usually determined by functions which calculate the actual values when called.
E.g., if you use

``myActionBuilder.setVisible(function(){ return State.get("abc") == 2 });``

the UI components associated with the action built from ``myActionBuilder`` are said to be visible to the user if and only if ``State.get("abc")`` returns ``2``.
Since the value may change over time, an update mechanism has been implemented, see [this section](#actionupdate) for details.

If an action is disabled, nothing will happen if the action's ``execute`` function is called.


###Creating new Actions

In order to create an Action, you can make use of an ``ActionBuilder`` (as shown in the example) by calling its setters first and calling ``buildAction()`` afterwards.

Note that, instead of passing functions to the setters, you can also use constants.
For example, instead of

``myActionBuilder.setEnabled(function(){ return true; });``

you can simply write

``myActionBuilder.setEnabled(true);``

to make sure that this action will always be enabled.


###Installing Actions

The created Action can be added to the UI depending on how and where this Action should be made available to the user.
For example, use ``Events.bindKeys()`` to make this Action available via keystrokes.

In the current implementation, the menu bar itself adds buttons to make its Actions available to the user.

###<a name="actionupdate">Installing listeners and the updating mechanism for Actions</a>

You can register listeners to an Action in order to get notified about changes that affect this Action (e.g. ``isEnabled`` now returns ``true`` instead of ``false``).

Since Actions may have values that change over time, an ``update`` function is provided.
You do not need to call this function explicitly since when an Action is created, it implicitly registers itself as a listener to the ``State`` object (and this listener calls this ``update`` function for you).
Similar to the ``update`` function of ``State`` object, this ``update`` function calls the listeners to this Action and automatically suppresses notifications about changes that are not actual changes (according to ``===``).

That is, you do not need to know the state variables a specific Action depends on.
Instead, you can simply register listeners directly to the Action of interest.

- - - -

##Widgets

###Overview

An Widget encapsulates

* an ID,
* the initialization function which is called when the widget is added to the document,
* a parent element, to which the widget is added,
* a position, and a
* visibility

###Adding new Widgets

In order to create a Widget, you call ``Widgets.addWidget`` with parameters: id, initialize-Function and a parent element.

###Showing/Hiding Widgets

The created Widget can be shown/hidden via calls to ``Widgets.show(parent)`` and ``Widgets.hide(parent)`` 

You can call it with the immediate parent or any other parent above, up to window.document

A Widget is not removed from its parent when it is set to invisible.

###Drag and Drop

To add Drag-and-Drop-functionality to a widget, call ``Widgets.addDragAndDrop(id, dropArea)``, where id is the
ID specified in addWidget.
Note that dropArea can be a HTMLElement or an array of HTMLElements

- - - -

## <a name="presentationwindow">PresentationWindow</a>

The PresentationWindow is technically a seperate instance of LectureDoc in a seperate window, but using the same HTML-Source.
It receives State-Updates from a Listener that is registered in the origin window.

To exchange data that is not kept in the State is send to the PresentationWindow via ``LectureDoc.sendData()``
``PresentationWindow.acceptData`` processes the known data, which is identified by a string, and ignores unknown data.

Every new feature that is not kept in the State, but should be tranferred to the PresentationWindow has to be send and processes accordingly.

Currently the following features are send:
* ASides
* MultipleChoice-Questions
* SimpleJS
* Laserpointer
* Canvas

But ``LectureDoc.sendData()`` can also used for other purposes, like hiding the Presentation-Menu-Hint.

- - - -

##Initialization

LD is initialized right after the DOM content has been (re-)loaded.
The current implementation takes four major steps in the following order to initialize LD,
after every step an event on the window is dispatched:
* ``analyzeDocument()``
    * The slides are decorated with automatically generated content. (Note: The current implementation simply paginates the document.)
    * The document is cloned for internal use.
    * The state object ("state") is updated for the first time in order to incorporate the properties as implied by the document.
    * fires a ``LDDocumentInitialized``-Event
* ``loadState()``
    * The state is updated using the currently stored locale storage data, if any.
    * The state is updated to incorporate the key-value pairs as specified by the URL.
    * An update mechanism is installed to keep the URL and the local storage in sync with the state.
    * fires a ``LDStateInitialized``-Event
* ``setupConstraints()``
    * Constraints are set and installed to facilitate debugging.
    * fires a ``LDConstraintsInitialized``-Event
* ``startUI()``
    * The event handling mechanisms (e.g. Mousetrap, Gator) are set up.
    * All UI components are initialized.
    * All predefined actions are installed (e.g. setting up key bindings).
    * fires a ``LDUIInitialized``-Event

**After a complete initialization an ``LDInitialized``-Event is fired.**

- - - -


##<a name="debugging">Pitfalls, troubleshooting and the debug mode</a>

LD has a few built-in functions to facilitate debugging:

* ``log(something, loglevel)``
* ``debugString(key, oldValue, newValue)``
* ``stringify(object)``

The ``log`` function generates output if and only if a variable called ``debug`` is set to a value smaller than or equal to the parameter ``loglevel``.
If ``loglevel`` is not specified, it is implicitly set to ``debug_INFO``.
There are several constants defined in LD to describe the different levels of importance of a debug message:

* ``debug_OFF = 0``
* ``debug_ERROR = -1``
* ``debug_WARNING = -2``
* ``debug_INFO = -3``
* ``debug_TRACE = -4``
* ``debug_ALL = -2147483648``

If quite complex code is necessary to generate a useful debug message or if you want to install listeners only for debugging purposes, you should use an appropriate ``if``-statement around it, e.g.:

``if (debug &lt;= debug_WARNING) { /* code to generate a warning */ }``

LD already makes use of the ``log`` function, whereas the most important log lines inform you about the following events.

* A ``debug_ERROR`` message is generated in the following cases:
    * If you attempt to read a variable that does not exist in the state, the result will be ``undefined``.
      Make sure that you did not accidently misspell variables.
    * If validation failed for a specific variable, this variable is not updated and an appropriate message appears in the logs.
* A ``debug_WARNING`` message is generated in the following cases:
    * Most likely, calling ``update`` within an update is a bug and might result in an undefined or illegal state.
      Both updates will be performed, though.
    * If you update the state with new variables, these variables are incorporated into the session state. Watch out for such messages because you might have accidently added misspelled variables.
    * Two listeners request conflicting changes.
* A ``debug_INFO`` message is generated in the following cases:
    * A constraint is registered.
    * Listeners are notified.
    * Listeners request changes.
    * The update section is being entered.
    * The number of the current update pass is logged.
    * A variable of the application state is updated.
    * A variable of the session state is updated.
    * If a listener wants to update the state, it must return an appropriate object in order to enqueue the requested changes.
      Note, however, that this might result in an infinite loop if two listeners mutually trigger themselves.
      Therefore, it is always a good idea to keep the number of update passes per ``update`` call as low as possible.
      The number of update passes is logged each time the update section is left.
    * A property of an action has changed (i.e. its value is not equal to the prior one according to ``===``).
* A ``debug_TRACE`` message is generated in the following cases:
    * Changing the value of a variable triggers validation.
    * A variable of the application state is read.
    * A variable of the session state is read.
    * An update of a variable is suppressed because its new value is equal to the prior one (according to ``===``).
    * An Action is about to notify its listeners about changes to its properties.
    * An Action suppresses an update for one of its properties because the new value is equal to the prior one (according to ``===``).
    * Two listeners request another update pass and they both request the same new value for a single variable (according to ``===``).

If ``debug`` does not evaluate to a value equivalent to ``false``, an appropriate message will appear just before the actual initialization of LD.  
**Make sure that debugging is turned off when you share your documents or start a lecture.
Otherwise, this message will reappear, lagging might occur and other inventive debugging messages might pop up when it is not appropriate.**

- - - -

##Third-party software

###jsdoc

``jsdoc`` extracts and compiles the comments for API documentation.
See the official websites for details:
[https://github.com/jsdoc3/jsdoc](https://github.com/jsdoc3/jsdoc) and [http://usejsdoc.org/](http://usejsdoc.org/)
The most recent version used here is:

``Version: JSDoc 3.2.0-dev (Fri, 19 Apr 2013 22:04:23 GMT)``

In this version of ``jsdoc``, error messages generated by ``jsdoc`` are sometimes difficult to read.
Note that, if you find line numbers in said messages, those line numbes usually refer to the code of ``jsdoc`` itself (pointing to the piece of code that is confused by your input) rather than the line numbers of the code you asked it to extract the documentation from.

If you want to use characters that are used by ``jsdoc``, you should use escape sequences.
The most interesting characters here are ``{`` and ``}`` which can be escaped using ``&amp;#123;`` and ``&amp;#125;``, respectively.

For some unknown reason, ``jsdoc`` does not accept an array of arrays as a type when written as ``[][]``.
Because of that, all arrays (even one-dimensional ones) are written as ``Array&lt;…&gt;``, e.g. ``Array&lt;Array&lt;Number&gt;&gt;`` instead of ``Number[][]``.

Another issue is that ``@memberof`` sometimes works only if it is not used in a single-line comment.
That is, you need write

``/** ``  
`` * @memberof SomeMember``  
`` */``

instead of

``/** @memberof SomeMember */``

It is unknown whether this applies only to this tag or other tags might be affected as well.
This bug might also have been fixed in the meantime.

For the current version of LD, ``jsdoc`` is called this way (working directory must be the one where the ``src/`` directory is located):

``jsdoc -p -d docs/jsdoc/ -r src/``

This ensures that private members are taken into account, the output directory is ``docs/jsdoc/`` and the source code to use are all Javascript files located in ``src/`` (recursively).
See ``jsdoc -h`` for details.

###js-beautify

``js-beautify`` is the beautifier for Javascript used for the Javascript code of LD.
See the official website for details:
[https://github.com/einars/js-beautify](https://github.com/einars/js-beautify)
The most recent version used here is:

``Version: 1.4.2``

In order to make sure that ``js-beautify`` does not accidentally scratch your work, you should make a backup of your code before you run the beautifier.
The following bash script automatically creates a backup and restores the files if something happens.
Since it is a bash script, it needs an appropriate environment (e.g. GNU/Linux or Cygwin) to work.
The working directory should be the one where the ``src/`` directory is located.
Otherwise, you need to modify this script accordingly.

    #!/bin/bash

    function beautify {
        mkdir -p "$JSBACKUPDIR"
        while [ "$1" != "" ] ; do
            if [ -f "$1" ] ; then
                echo "copying $1"
                cp -a --parents "$1" "$JSBACKUPDIR"
                if [ "$?" != "0" ] ; then
                    echo "could not create a copy, $1 skipped"
                else
                    echo "beautifying $1"
                    # using -o doesn't work, so we must redirect stdout
                    js-beautify -j -k -s 4 -w 90 "$JSBACKUPDIR/$1" > "$1"
                    if [ "$?" != "0" ] ; then
                        echo "something went wrong during beautifying, reverting $1"
                        cp -a "$JSBACKUPDIR/$1" "$1"
                    fi
                fi
            else
                echo "$1 is not a regular file, skipped"
            fi
            shift
        done
    }

    JSBACKUPDIR="js-beautify_BAK"

    beautify \
    "src/main/resources/Library/LectureDoc/LectureDoc-Help.js" \
    "src/main/resources/Library/LectureDoc/LectureDoc.js" \
    "src/main/resources/Library/Timeline-1.0.0/Timeline.js" \
    "src/main/resources/Library/MultipleChoice-1.0.0/MultipleChoice.js" \
    ""

###Mousetrap

``Mousetrap`` is a library for simplified, cross-browser handling of keyboard events. See the official website for details: [http://craig.is/killing/mice](http://craig.is/killing/mice). The most recent version used here is:

``Version: 1.4.6``

``Mousetrap`` is used to handle keyboard shortcuts that are defined by actions. The primary reason to integrate it was its built-in support for the ``meta`` key, which equals ``ctrl`` on Windows and Linux and ``cmd`` on Mac OS.

###Gator

``Gator`` is a general purpose event dispatching library. See the official website for details: [http://craig.is/riding/gators](http://craig.is/riding/gators). The most recent version used here is:

``Version: 1.2.2``

``Gator`` is used mostly to reduce the number of click listeners in the application. Secondarily, ``Gator`` is used to handle several special cases of keyboard input that cannot easily be covered using ``Mousetrap``.

Example: jumping to another slide by entering its number followed by the enter key requires capturing a dynamic key sequence. While it would be possible to implement the same behavior using an action and Mousetrap shortcuts, usage of the enter key as "next slide" when no slide number has been entered would have to be merged into that code.

Second example: triggering modes via shortcuts m+Letter is not possible via ``Mousetrap`` when at the same time m should trigger the modes sub menu. That is because ``Mousetrap`` cannot handle shortcuts that have a "normal" action and are the start of a static keyboard sequence.  
Setting the mode actions to enabled only if the modes sub menu is open and setting their shortcuts to the respective letter ``(p,d,n,l,c)`` would work for all cases except l, since the l key is already bound to the "last slide" action. Therefore it has been decided to implement the mode triggering second key with ``Gator``.

###MathJax

``MathJax`` is a cross-browser library for displaying mathematical notation in web browsers, using MathML, LaTeX and ASCIIMathML markup. See the official website for details: [http://www.mathjax.org/](http://www.mathjax.org/). The most recent version used here is:

``Version: 2.3``

Within the scope of LD ``MathJax`` is configured to allow specifying math in TeX or LaTeX notation (indicated by math delimiters like ``$$ ... $$``). ``MathJax`` purpose is displaying math, therefore it only supports the subsets of TeX or LaTeX used to describe mathematical notation.

###Highlight.js

``Highlight.js`` is a cross-browser library for highlighting syntax in code examples on web pages. See the official websites for details: [https://github.com/isagalaev/highlight.js](https://github.com/isagalaev/highlight.js) and [http://highlightjs.org/](http://highlightjs.org/). The most recent version used here is:

``Version: 8.0``

``Highlight.js`` is used to highlight syntax within ``<code>...</code>`` fragments. By specifying an optional language class after the start of a fencedCode definition marked by <code>\`\`</code> in markdown documents, it is possible to set a code fragment's language explicitly. As recommended in the [HTML Living Standard](http://www.whatwg.org/specs/web-apps/current-work/multipage/text-level-semantics.html#the-code-element) those language tokens should be prefixed with "language-". A list of supported languages including their keywords can be found at ``src/main/resources/Library/highlight.js-8.0/css-classes-reference.rst``.

First Example: Parsing  
<code>``language-java<br>
...<br>
``
</code>


would result in ``<pre><code class="language-java">...</code></pre>``. In this case, ``Highlight.js`` would highlight the code fragment using ``Java`` syntax. To disable highlighting of a fragment altogether use "no-highlight".

If no language class is specified, ``Highlight.js`` automatically finds blocks of code, detects their language based on heuristics and highlights them.

- - - -

Last Modified: 2014-03-23 16:24:00 CET  
Authors: Daniel Killer, Arne Lottmann, Tobias Becker, David Becker