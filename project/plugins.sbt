resolvers += Resolver.url(
  "sbt-plugin-releases", 
  new URL("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases/")
)(Resolver.ivyStylePatterns)

addSbtPlugin("com.github.retronym" %% "sbt-onejar" % "0.8")

resolvers += Resolver.url(
  "untyped",
  url("http://ivy.untyped.com")
)(Resolver.ivyStylePatterns)

addSbtPlugin("com.untyped" % "sbt-less" % "0.6-M5")