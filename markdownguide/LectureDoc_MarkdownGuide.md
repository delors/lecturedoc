![Logo](Images/logoUserGuide.png)
- - -

+~[What is LectureDoc?]slide

```markdown
**A short introduction**
# What is LectureDoc?
**LectureDoc** is a system for producing documents that combine lecture slides and a lecture script out of a single file.

It enables you to access those documents through different display modes, catering to the different needs of both lecturer and those attending the lecture.
```

~+
**A short introduction**   {formatting:no_margin}
# What is LectureDoc?   {formatting:markdownguide_h3}
**LectureDoc** is a system for producing documents that combine lecture slides and a lecture script out of a single file.  

It enables you to access those documents through different display modes, catering to the different needs of both lecturer and those attending the lecture.


+~slide
```markdown
**LectureDoc** consists of three separate parts:

##Part 1
A [Markdown](http://daringfireball.net/projects/markdown/) dialect in which you write your documents.

##Part 2
A parser, written in [Scala](http://www.scala-lang.org/), that transforms your documents into valid HTML5.
```
~+
**LectureDoc** consists of three separate parts:

##Part 1   {formatting:markdownguide_h4}
A [Markdown](http://daringfireball.net/projects/markdown/) dialect in which you write your documents.

##Part 2   {formatting:markdownguide_h4}
A parser, written in [Scala](http://www.scala-lang.org/), that transforms your documents into valid HTML5.

+~slide
```markdown
##Part 3
The user interface, written in JavaScript and CSS. Some of the features it offers you are: 
* different display modes specialized for certain tasks ( presenting, studying )
* optional slide transitions and displaying elements in your desired order
* printing your documents
```
~+
##Part 3   {formatting:markdownguide_h4}
The user interface, written in JavaScript and CSS. Some of the features it offers you are: 
* different display modes specialized for certain tasks ( presenting, studying )
* optional slide transitions and displaying elements in your desired order
* printing your documents

+~[Why should I use LectureDoc?]slide

```markdown
# Why should I use LectureDoc?
**LectureDoc** combines the best of two worlds. On one side LectureDoc is a Markdown dialect, allowing you to separate the content and structure
of your document from the formatting. While you _just focus on writing_, LectureDoc takes care of the rest.

The goal is to be as compatible as possible with [Markdown](http://daringfireball.net/projects/markdown/syntax#link) and common extensions, such as [MultiMarkdown](http://fletcherpenney.net/multimarkdown/), to leverage existing tool support.

However, some features are added to facilitate the creation and presentation of a slide set. In particular support was added for being able to define slides with additional features like slide transitions.
```
~+

# Why should I use LectureDoc?   {formatting:markdownguide_h3}
**LectureDoc** combines the best of two worlds. On one side LectureDoc is a Markdown dialect, allowing you to
separate the content and structure of your document from the formatting. While you _just focus on writing_, LectureDoc takes care
of the rest.

The goal is to be as compatible as possible with [Markdown](http://daringfireball.net/projects/markdown/syntax#link) and common extensions, such as [MultiMarkdown](http://fletcherpenney.net/multimarkdown/), to leverage existing tool support.

However, some features are added to facilitate the creation and presentation of a slide set. In particular support was added for being able to define slides with additional features like slide transitions.

+~slide

```markdown
## No need for additional tools!
But **LectureDoc** also shines after you have created your document! A wide selection of _display modes_ allow LectureDoc to be customizable to _fit your needs_. 
The same document you use in a lecture can also be used by students as a script to prepare for assignments and exams.

## Spice it up!
Beyond the scope of standard Markdown it's also possible to define slide transitions, include Math via [MathJax](http://www.mathjax.org/) and do much more!
```
~+

## No need for additional tools!   {formatting:markdownguide_h4}
But **LectureDoc** also shines after you have created your document! A wide selection of _display modes_ allow LectureDoc to be customizable to _fit your needs_. The same document you use in a lecture can also be used by students
as a script to prepare for assignments and exams.

## Spice it up!   {formatting:markdownguide_h4}
Beyond the scope of standard Markdown it's also possible to define slide transitions, include Math via [MathJax](http://www.mathjax.org/) and do much more!

+~[Section Blocks]slide

**Typical markdown example of a slide set:**

<pre><code class="markdown"><pre data-formatting="no_margin">+~[Hello World]slide{transition:blendOver;duration:1500}</pre>
Welcome to the world of **LectureDoc**!

+~[Hello Aside]aside
I'm an aside!
~+
 
~+

A "Hello World" program has become the traditional first program
that many people learn.
</code></pre>

+~[Hello Aside]aside
_Hello, I'm an aside!_   {animation-step:3}
~+

~+

# Creating your own Slide Set   {formatting:markdownguide_h3}

To the right you see a typical example of creating a **slide set** in LectureDoc. It may seem a bit complicated at first, but this chapter is aimed
at making you understand how to create _slide sets_ easily yourself.

## Section Blocks   {formatting:markdownguide_h4}

**Section blocks** are the main building blocks for giving structure to your document.

```markdown   {formatting:syntaxbackground}
 +~[title]class{customDataAttributes}
 sectionContent
 ~+  	  
```

+~slide
**Typical HTML example of a slide set:**

```markdown
 <pre><section class="slide" data-title="Hello World"
 data-transition="blendOver" data-duration="1500">
 <div class="section-body">
 <p>Welcome to the world of <strong>LectureDoc</strong>!</p>

 <aside data-title="Hello Aside"><div class="aside-body">
 <p>I'm an aside!</p></div></aside></div></section>

 <p>A "Hello World" program has become the traditional first
 program that many people learn.</p>
```

~+

A _section block_ starts with a **section start line**. _Section start lines_ begin with **"+~"** and are optionally followed by a **title**, **class** and **customDataAttribute**.
Within a _section block_ you can add your content. Pictures, paragraphs, everything goes here! You end _section blocks_ with a **section end line**. They begin with **"~+"** and are optionally
followed by white space.

_Title_ and _class_ of a _section block_ are optional, but are great tools for giving structure to your document.

In the case of slides, adding a _title_ to a _section block_ shows this slide in **Key Slides**, an overview of important slides. Press **'K'** for a live demo!

+~slide

**Example of a lecturer note within a slide:**

```markdown
+~[Slide with lecturer note]slide

+~[Lecturer note within slide]note
Might add an anecdote here...
~+

A "Hello World" program has become the traditional first program
that many people learn.

~+
```

+~[Lecturer note within slide]note
Might add an anecdote here...
~+

~+

_Class_ can be one of the following:

* **slide**: The main components of your _slide set_, used whenever you want to presentate content. Note that content between two _slides_   {formatting:smallersize}
is only accessible in Document Mode and Notes Mode.   {formatting:smallersize}
* **aside**: Used for providing additional information on slides, aimed at those attending the lecture. _Title_ is mandatory   {formatting:smallersize}
if you want _asides_ to be accessible in your document!
* **note**: Used for prodiving the lecturer with additional information, guiding him through his presentation.   {formatting:smallersize}

You define aside and note section blocks within slide section blocks. More detailed examples can be found in _Chapter 4 - Syntax_.

+~slide

**Example usage of customDataAttributes within a section slide:**

<pre><code class="markdown"><pre data-formatting="no_margin">+~[customDataAttributes]slide{foo:bar;bar-foo:500}</pre>
Using **LectureDoc** customDataAttributes in paragraphs.<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:1}

#And in headlines!<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:2}

~+
</pre></code>

~+

## customDataAttributes   {formatting:markdownguide_h4}

**customDataAttributes** are a mechanism for specifying additional functionality in LectureDoc. They are very flexible and let you add slide transitions to _slide blocks_ or 
choose the order in which elements within _slide blocks_ are displayed.

```markdown   {formatting:syntaxbackground}
key:value[;key:value...]
```

If you decide to add _customDataAttributes_, you have to specify at least one _customDataAttribute_ enclosed in curly braces. Additional _customDataAttributes_ are delimited by semicolons.

+~slide

**Further example usage of customDataAttributes within a section slide:**

<pre><code class="markdown"><pre data-formatting="no_margin">+~[customDataAttributes]slide</pre>
* Adding customDataAttributes to lists.<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{foo:bar}

```<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{lecture:doc;foo-bar:5}
Adding customDataAttributes to code blocks.
```

~+
</pre></code>

~+

**key** has to **start with one word character** ( a-z, A-Z, 0-9 ) and is followed by any number of additional word characters or '-' / '_'. **value** consists of **at least one word character**
followed by any number of additional word characters.

If you want to add _customDataAttributes_ within _sectionContent_ you have to add them at the end of lines, enclosed in curly braces and preceded by three whitespaces.

+~[Syntax]slide
<pre><code class="markdown"># Syntax + Advanced Features
The markdown to the right helps you understand how the result to the left was achieved. This makes it very easy for you to get to know
all features **LectureDoc** supports and _use them yourself_!

Some features like slide transitions offer an advanced introduction beyond pure examples.

If it's possible to combine _customDataAttributes_ with an advanced feature the example shows you how to utilize them.

<font data-formatting="whitespace">&nbsp;</font> denotes whitespaces.
</pre></code>
~+

# Syntax + Advanced Features   {formatting:markdownguide_h3}

The markdown to the right helps you understand how the result to the left was achieved. This makes it very easy for you to get to know
all features **LectureDoc** supports and _use them yourself_!

Some features like slide transitions offer an advanced introduction beyond pure examples. 

If it's possible to combine _customDataAttributes_ with an advanced feature the example shows you how to utilize them.


+~[Slide Transitions]slide
**Example usage of a blendOver slide transition:**
<pre><code class="markdown"><pre data-formatting="no_margin">+~[Slide Transitions]slide{transition:blendOver;duration:10000}</pre>
That's the new content to slowly blend over the old content.
10000ms sure takes a while!

~+
</code></pre>

~+

## Slide Transitions   {formatting:markdownguide_h4}

**Live demo:** Click on the icon below and use your arrow keys to navigate through the presentation!
<div id="wrap">
<iframe id="frame" src="MarkdownGuide_Transitions.html#mode=presentation&currentSlide=0&doNotShowMenuHint=true&presentationZoomFactor=1&currentAnimationStep=-1,-1,-1,-1,-1,-1,-1"></iframe>
</div>

+~slide
**Example usage of slideOut/dissolve slide transitions:**
<pre><code class="markdown"><pre data-formatting="no_margin">+~[Slide Transitions]slide{transition:slideOut;direction:up}</pre>
Goodbye old content. Even though it only took half a second, seeing you
slide out towards the top surely left an impression.
~+
</code></pre>

<pre><code class="markdown"><pre data-formatting="no_margin">+~[Slide Transitions]slide{transition:dissolve;duration:1000ms}</pre>
Dissolve, old content!
~+
</code></pre>

~+

You can define **slide transitions** by using _customDataAttributes_ in _section start lines_.

```markdown   {formatting:syntaxbackground}
transition:type[;duration:value]
```

**Transition** can be one of the following three types: _slideOut_, _dissolve_, _blendOver_. If you choose _slideOut_ 
you can optionally specify the direction as one of the possible values _left_, _right_, _up_ and _down_. Direction defaults to _right_.

**Duration** is optional and influences the duration of slide transitions in milliseconds. If duration isn't specified, 500ms
is chosen as default.

+~[Slide Transitions]slide
**Example usage of element animation:**
<pre><code class="markdown"><pre data-formatting="no_margin">+~[Element Animation]slide</pre>
#How to animate elements?
* Showing elements in the order you want<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:1}
* is easy to do with LectureDoc!<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:2}

![Logo](Images/logo.png)<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:3}
<pre data-formatting="no_margin">~+</pre>
</code></pre>

~+

## Element Animation   {formatting:markdownguide_h4}

**Live demo:** Click on the icon below and use your arrow keys to navigate through the presentation!

<div id="wrap">
<iframe id="frame" src="MarkdownGuide_ElementAnimation.html#mode=presentation&currentSlide=0&doNotShowMenuHint=true&presentationZoomFactor=1&currentAnimationStep=-1,-1"></iframe>
</div>

+~[Element Animation]slide
**Example usage of element animation with gaps:**
<pre><code class="markdown"><pre data-formatting="no_margin">+~[Element Animation]slide</pre>
#Did you know?<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:10}

* Displaying content<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:7}
* in a reversed order<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:2}

makes it hard for students to follow.<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>{animation-step:2}
<pre data-formatting="no_margin">~+</pre>
</code></pre>
~+

You can use **element animation** by using _customDataAttributes_ at the end of lines within _sectionContent_ of _slides_.

```markdown   {formatting:syntaxbackground}
animation-step:value
```

**value** has to be a positive number. It defines the order in which elements are displayed in Presentation Mode. Elements having 
the same value are displayed simultaneously. All elements without a value are displayed from the start.

Elements are displayed in an _ascending order_ with regard to their value. It is also possible to leave gaps, e.g. using values 1 and 3 but skipping 2.

+~[Multiple Choice]slide

**Example usage of multiple choice questions:**

	<div id="question"></div>
	<script type="text/javascript">
		MultipleChoice.showQuestion("question",
		  "Which numbers are multiples of 3?", [
			{a: "7", c: false, e: "7=2*3+1 \u2717"},
			{a: "9", c: true, e: "9=3*3 \u2713"},
			{a: "211", c: false, e: "211=70*3+1 \u2717"},
			{a: "2511", c: true, e: "2511=837*3 \u2713"}
		]);
	</script>
~+

## Multiple Choice   {formatting:markdownguide_h4}
<div id="question"></div>
<script type="text/javascript">
	MultipleChoice.showQuestion("question", 
	"Which numbers are multiples of 3?", [
		{a: "7", c: false, e: "7=2*3+1 \u2717"},
		{a: "9", c: true, e: "9=3*3 \u2713"},
		{a: "211", c: false, e: "211=70*3+1 \u2717"},
		{a: "2511", c: true, e: "2511=837*3 \u2713"}
	]);
</script>


+~[SVG]slide

**Example usage of SVG with optional element animation:**

```html
<svg width="150" height="150">
 <circle cx="50" cy="50" r="40" stroke="red" stroke-width="4"
   fill="blue"/>
 <circle data-animation-step="2" cx="90" cy="90" r="40" stroke="yellow"
   stroke-width="4" fill="red"/>
</svg>
```

~+

## SVG   {formatting:markdownguide_h4}

<svg width="150" height="150">
  <circle cx="50" cy="50" r="40" stroke="red" stroke-width="4" fill="blue"/>
  <circle data-animation-step="2" cx="90" cy="90" r="40" stroke="yellow" stroke-width="4" fill="red"/>
</svg>

Element animation can be done manually like stated in the example.


+~[MathJax]slide
**Example usage of MathJax:**

```
$$ x^{p-1} \equiv 1 mod p $$
$$ e^{i\pi} + 1 = 0 $$
```

~+

## MathJax   {formatting:markdownguide_h4}

$$ x^{p-1} \equiv 1 mod p $$
$$ e^{i\pi} + 1 = 0 $$

[MathJax](http://www.mathjax.org/) allows you to _display math_! **$$** are used as delimiters to indicate math
specified in TeX or LaTeX notation.

+~[Asides]slide
**Example usage of asides:**

```markdown
+~[Slide with aside]slide

Hello LectureDoc!

+~[Aside within slide]aside
Follow LectureDoc at [Bitbucket](https://bitbucket.org/delors/lecturedoc)
~+

~+
```

+~[Aside within slide]aside

Follow LectureDoc at [Bitbucket](https://bitbucket.org/delors/lecturedoc)

~+

~+

## Asides   {formatting:markdownguide_h4}

As seen in _Chapter 3.1 - Section Blocks_, **asides** are _section blocks_. In order to utilize _asides_, 
you have to specify a title and define _asides_ within _slides_, otherwise you won't be able to access them in Presentation Mode.

By displaying an extra window in the presentation mode, _asides_ enable the lecturer to embed additional information on slides.
Asides are also visible in Document Mode and Notes Mode, as can be seen by looking to the right!

+~[Lecturer Notes]slide
**Example usage of lecturer notes:**

```markdown
+~[Slide with lecturer note]slide

Hello LectureDoc!

+~[Lecturer note within slide]note
Remind my students that they should follow LectureDoc at [Bitbucket](https://bitbucket.org/delors/lecturedoc)
~+

~+
```

+~[Lecturer note within slide]note
Remember my students that they should follow LectureDoc at [Bitbucket](https://bitbucket.org/delors/lecturedoc)
~+

~+

## Lecturer Notes   {formatting:markdownguide_h4}

**Lecturer notes** are _section blocks_. In order to utilize _lecturer notes_ you have to define them within _slides_,
otherwise you won't be able to access them in Presenter Mode.

_Lecturer notes_ are used for prodiving the lecturer with additional information, guiding him through his presentation.
They can be accessed by enabling the Presenter Mode by pressing **F8** in Presentation Mode and are displayed beneath the
current slide. Lecturer notes are also visible in Document Mode and can be toggled by pressing **'P**!

+~[Text]slide
**Example usage of text formatting:**
```markdown
This text is **bold**.
```

```markdown
This text is _italicized_.
```

```markdown
`This is some code.`
```

<pre><code class="markdown"><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>This is a block of code
<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>spanning multiple lines.
<font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font><font data-formatting="whitespace">&nbsp;</font>Also works with tabs.
</code></pre>

 
</code></pre>

~+

## Text   {formatting:markdownguide_h4}

This text is **bold**.

This text is _italicized_.

`This is some code.`

    This is a block of code   {formatting:defaultbackground}
	spanning multiple lines.
    Also works with tabs.

+~[Links]slide
**Example usage of links:**
```markdown
Link to [TU Darmstadt](http://www.tu-darmstadt.de/ "Optional  title").
```

```markdown
Link to <http://www.tu-darmstadt.de>.
```

<pre><code class="markdown">Link to [Google] with a definition later on.

[Referencing an URL by ID][Google].

[Google]: http://www.google.com
</code></pre>

~+

[Google]: http://www.google.com

##Links   {formatting:markdownguide_h4}

Link to [TU Darmstadt](http://www.tu-darmstadt.de/ "Optional  title").

Link to <http://www.tu-darmstadt.de>.

Link to [Google] with a definition later on.

[Referencing an URL by ID][Google].

<pre data-formatting="syntaxbackground"><code class="markdown">[ID]: value</code></pre>

+~[Blockquotes]slide
**Example usage of blockquotes:**
```markdown

> "The significant problems we face cannot be solved at the same level of thinking we were at when we created them."
>
> Einstein
```

~+

##Blockquotes   {formatting:markdownguide_h4}

> "The significant problems we face cannot be solved at the same level of thinking we were at when we created them."
>
> Einstein

<pre data-formatting="syntaxbackground"><code class="markdown">>[ ]content</code></pre>

+~[Statements]slide
**Example usage of statements:**
```markdown
^**Style can only be recommended, not enforced!**
```

~+

##Statements   {formatting:markdownguide_h4}

^**Style can only be recommended, not enforced!**

<pre data-formatting="syntaxbackground"><code class="markdown">^[ ]content</code></pre>

+~[Images]slide
**Example usage of images:**
```markdown
Simple example of ![img](Images/example_image.jpg) embedded in paragraph.
```

```markdown
This time ![img2](Images/example_image.jpg "Optional title") is embedded with a title attribute.
```

```markdown
The same image ![img3][] with attributes defined later on.
```

~+

[img3]: Images/example_image.jpg "Optional title"

##Images   {formatting:markdownguide_h4}

Simple example of ![img](Images/example_image.jpg) embedded in paragraph.

This time ![img2](Images/example_image.jpg "Optional title") is embedded with a title attribute.

+~[Horizontal rule]slide
<br><br><br><br><br><br>
**Example usage of horizontal rules:**
```markdown
- - -

-	-	-	-	-
```
~+

The same image ![img3][] with attributes defined later on.

See _Links_ for an example of attributes that are defined later on.

##Horizontal rule   {formatting:markdownguide_h4}
- - -
<br>
-	-	-	-	-

+~[Lists]slide
**Example usage of unordered lists:**
```
* Point 1
* Point 2
    * Point 2.1
        * Point 2.1.1
        * Point 2.1.2
```

~+


##Lists   {formatting:markdownguide_h4}

* Point 1
* Point 2
    * Point 2.1
        * Point 2.1.1
        * Point 2.1.2

+~slide
**Example usage of ordered lists:**
```markdown
1. Point 1
2. Point 2
    1. Point 2.1
        1. Point 2.1.1
        2. Point 2.1.2
```

~+

1. Point 1
2. Point 2
    1. Point 2.1
        1. Point 2.1.1
        2. Point 2.1.2

+~[Fenced Code Blocks]slide
**Example usage of fenced code blocks:**
```markdown
 ```java
 class HelloWorldApp {
     public static void main(String[] args) {
         System.out.println("Hello World!"); // Display the string.
     }
 }
 ```
```
~+

##Fenced Code Blocks   {formatting:markdownguide_h4}

```java   {formatting:defaultbackground}
class HelloWorldApp {
    public static void main(String[] args) {
        System.out.println("Hello World!"); // Display the string.
    }
}
```

<pre data-formatting="syntaxbackground"><code class="markdown">\`\`\`[languageToken][whitespace]
code
```</code></pre>

**languageToken** is optional and allows you to specify a language for your code block, thanks to [HighlightJS](http://highlightjs.org/). 
See `src/main/resources/Library/highlight.js-8.0/css-classes-reference.rst` for a list of available languages.

+~[Headings]slide
**Example usage of headings:**
```markdown
#Level 1 Header
##Level 2 Header
###Level 3 Header
####Level 4 Header
#####Level 5 Header
######Level 6 Header
```

~+

##Headings   {formatting:markdownguide_h4}

#Level 1 Header
##Level 2 Header
###Level 3 Header
####Level 4 Header
#####Level 5 Header
######Level 6 Header

+~slide
**Example usage of setext headings:**
```markdown

Setext Level 1 Header
=====

Another setext Level 1 Header
==========

Setext Level 2 Header
-----

Another setext Level 2 Header
--
```

~+

##Setext Headings   {formatting:markdownguide_h4}

Setext Level 1 Header
=====

Setext Level 2 Header
-----

<pre data-formatting="syntaxbackground"><code class="markdown">header
=[=...][whitespace]<br>
header
-[-...][whitespace]</code></pre>

+~[Compiling & Launcher]slide
**Command for manually compiling the whole project:**
```markdown
sbt one-jar
```

**Example for manually generating guide.html from guide.md:**
```java
java -jar lecturedoc_2.10-0.0.0-one-jar.jar --complete <guide.md >guide.html
```
~+

# Compiling LectureDoc & Launcher   {formatting:markdownguide_h3}

To build **LectureDoc** you need a working installation of [sbt](http://www.scala-sbt.org/). To build a single jar that contains all required jars and which can be started use `sbt one-jar` in the main directory ( where `build.sbt` is located ).
This .jar can be found in `lecturedoc/target/scala-2.10`.

Afterwards you can execute _LectureDoc_ by passing in a document via the system input stream. _LectureDoc_ will then generate a HTML5 Document. 

_LectureDoc_ requires `scala-2.10/classes/Library` to be in the same directory where the converted file (HTML) is stored. See `LectureDocHelp.txt` for more info!

+~slide
**LectureDoc launcher in use:**

![LectureDoc Launcher](Images/launcher.png)
~+

Located in the main directory, **LectureDoc.jar** speeds up your process of converting your documents.

* **Complete**: If chosen, complete HTML documents are created that can be opened in a browser. This requires the library folder.   {formatting:smallersize}
* **Convert .md file**: A single _LectureDoc_ file ("*.md") is converted to HTML.   {formatting:smallersize}
* **Watch Directory / Stop**: The specified folder is watched for changes and additions of _LectureDoc_ files ("*.md").   {formatting:smallersize}
When a change/addition is detected the file is automatically converted to HTML.   {formatting:smallersize}