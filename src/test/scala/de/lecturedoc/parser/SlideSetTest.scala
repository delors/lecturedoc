/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc
package tools

import java.io.{ StringReader, BufferedReader, FileInputStream, BufferedInputStream, ByteArrayOutputStream, File }
import java.util.Date

import scala.io.Source._
import scala.util.matching.Regex

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers

import de.lecturedoc.parser.DefaultLectureDocTransformer

/**
  * Tests the process of reading LectureDoc document from a given `InputStream` and writing the generated HTML5 file to an
  * `OutputStream`. As sample documents the Software Engineering Design & Construction WS2013/2014 slide set is used. 
  * The results of the process are then checked for equality with already existing documents created with a previous LectureDoc version.
  */
@RunWith(classOf[JUnitRunner])
class SlideSetTest extends FlatSpec with ShouldMatchers {

    val path = new File("src/test/scala/documents/")
    
    // regex for stripping output of its data-hash attributes
    val datahash : Regex = """( data-hash=\"){1}\w+\"""".r

    for(file <- path.listFiles if file.getName endsWith ".md"){

        // convert .md files to .html
        val is = new BufferedInputStream(new FileInputStream(file))
        val os = new ByteArrayOutputStream
        process(path, is, DefaultLectureDocTransformer, os, false, new Date)
        val newDocument = new String(os.toByteArray,"UTF-8").mkString
        val newDocumentReader = new BufferedReader(new StringReader(newDocument))

        // get correponding existing converted document from /documents
        val originalDocument = fromFile(path+File.separator+file.getName.stripSuffix(".md")+".html")("UTF-8").getLines

            "LectureDoc" should ("parse the md file "+file.getName+" identically to the already parsed html file "+file.getName.stripSuffix(".md")+".html") in {
            // test if the two documents are equal
            for(originalDocumentLine <- originalDocument){
            var newDocumentLine = datahash.replaceAllIn(newDocumentReader.readLine, "")
            originalDocumentLine should equal(newDocumentLine)
            }
        }
    }
}