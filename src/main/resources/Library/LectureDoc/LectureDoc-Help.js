LectureDoc.setHelp({
    content: [
        {
            "show this help": ["h"],
            "select rendering mode": ["m"],
            "switch to presentation mode": ["m + p"],
            "switch to document mode": ["m + d"],
            "switch to notes mode": ["m + n"],
            "switch to light table mode": ["m + l"],
            "switch to continuous mode": ["m + c"]

  },
        {
            "go to first slide": ["f"],
            "go to last slide": ["l"],
            "go to next slide": ["right", "down", "page down", "space bar", "enter"],
            "go to previous slide": ["left", "up", "page up"],
            "go to slide": ["[0...9]<sup>*</sup> + enter"],
            "toggle transitions between slides": ["z"]
  },
        {
            "toggle key slides": ["k"],
            "toggle table of contents": ["t"],
            "toggle overview": ["o"],
            "toggle asides": ["a"],
            "toggle lecturer notes": ["x"]
  },
        {
            "toggle blackout": ["b"],
            "toggle whiteout": ["w"],
            "toggle element animation": ["v"],
            "toggle virtual laser pointer": ["ctrl + &lt;mouse movement&gt;"],
            "toggle client notes menu": ["n"],
            "toggle canvas menu": ["c"],
            "draw line if canvas is active": ["shift + &lt;mouse movement&gt;"]
  },
        {
            "toggle presentation mode menu": ["click on slide number"],
            "toggle enhanced presenter mode": ["F8"],
            "toggle extra window": ["F9"],
  },
        {
            "toggle full screen mode": ["browser dependent (usually F11)"]
  }

 ],
    footer: "LectureDoc © 2013, 2014 Michael Eichberg, Marco Jacobasch, Arne Lottmann, Daniel Killer, Simone Wälde, Kerstin Reifschläger, Andre Pacak, Tobias Becker, David Becker, Volkan Hacimüftüoglu"
});
