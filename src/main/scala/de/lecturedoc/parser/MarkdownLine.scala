/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc
package parser

import scala.language.postfixOps

import scala.util.parsing.input.{ Position, Reader }
import java.util.StringTokenizer
import scala.collection.mutable.{ HashMap, ArrayBuffer, ListBuffer }

/**
  * Represents a line of markdown.
  *
  * The prefix is the beginning of the line that indicates the line type,
  * the payload is the actual content after the prefix followed by optional
  * customDataAttributes containing additional information about the line.
  */
sealed abstract class MarkdownLine(val prefix: String, val payload: String, val attributes: Option[List[CustomDataAttribute]] = None) {

    /**
      * Constructs a MarkdownLine where the prefix is the empty String and the
      * payload is the whole line.
      */
    def this(c: String) = this("", c)

    /**
      * Constructs a MarkdownLine where the prefix is the empty String and
      * the whole line consists of payload + attributes.
      */
    def this(c: String, a: Option[List[CustomDataAttribute]]) = this("", c, a);

    /**
      * Returns the full line as it was originally, i.e. prefix+payload+attributes
      */
    def fullLine = prefix + payload + attributesToString(attributes)

    /**
      * Returns the full line as it was originally, stripped of its attributes, i.e. prefix+payload
      */
    def fullLineWithoutAttributes = prefix + payload

    /**
      * Returns the full line as it was originally, stripped of its prefix, i.e. payload+attributes
      */
    def fullLineWithoutPrefix = payload + attributesToString(attributes)

    /**
      * Helper for restoring customDataAttributes to its original form in the parsed string
      */
        def attributesToString(attributes: Option[List[CustomDataAttribute]]): String =  {
        attributes match {
            case Some(a: List[CustomDataAttribute]) => "   {" + attributes.map(cdas => cdas.map(_.toLectureDocAttribute).mkString(";")).getOrElse("") + "}"
            case _ => ""
        }
    }


}

/**
  * Represents lines of verbatim xml.
  *
  * Actually this class is a little cheat, as it represents multiple lines.
  * But it is a token that is created when "parsing with a line scope", so it is not too bad.
  */
case class XmlChunk(content: String) extends MarkdownLine(content)

/**
  * Represents the underline for a setext style header
  */
case class SetExtHeaderLine(content: String, headerLevel: Int, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(content, customDataAttributes)

/**
  * An atx style header line.
  *
  * Trims hashes automatically and determines the header level from them.
  */
case class AtxHeaderLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes) {

    /**
      * Removes all whitespace, nl and trailing hashes from the payload
      * "  foo ##  \n" => "foo"
      */
    def trimHashes() = {
        val s = payload.trim
        var idx = s.length - 1
        while (idx >= 0 && s.charAt(idx) == '#') idx -= 1
        s.substring(0, idx + 1).trim
    }

    def headerLevel = prefix.length
}

/**
  * A line indicating a statement (starts with "^")
  */
case class StatementLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes)

/**
  * A line consisting only of whitespace.
  */

case class EmptyLine(content: String) extends MarkdownLine(content)

/**
  * A horizontal ruler line.
  */
case class RulerLine(content: String) extends MarkdownLine(content)

/**
  * A line indicating a block quote (starts with "> ")
  */
case class BlockQuoteLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes)

/**
  * A line indicating the start of an unordered list item (starts with "   *  ")
  */
case class UItemStartLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes)

/**
  * A line indicating the start of an ordered list item (starts with "   [NUMBER].  ")
  */
case class OItemStartLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes)

/**
  * A line in verbatim code or the continuation of a list item
  */
case class CodeLine(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes)

/**
  * Starting line of a fenced code block: three backticks followed by a language token (e.g., "Java").
  */
case class ExtendedFencedCode(pre: String, pay: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, pay, customDataAttributes) {
        def language = pay.trim()
    }

/**
  * Starting or ending line of a fenced code block: three backticks followed by optional whitespace
  */
case class FencedCode(pre: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(pre, "", customDataAttributes)

case class CustomDataAttribute(key: String, value: String) {
    def toLectureDocAttribute = key+":"+value
    def toHTML5DataAttribute = "data-"+key+"=\""+value+"\""
}

case class SectionStart(
    pre: String,
    sectionTitle: Option[String],
    sectionClass: Option[String],
    customDataAttributes: Option[List[CustomDataAttribute]])
        extends MarkdownLine(
            pre,
            sectionTitle.map("["+_+"]").getOrElse("") +
                sectionClass.getOrElse("") +
                customDataAttributes.map(_.map(_.toLectureDocAttribute).mkString(";")).getOrElse(""))

case class SectionEnd(pre: String) extends MarkdownLine(pre)

/**
  * Any other line.
  */
case class OtherLine(content: String, customDataAttributes: Option[List[CustomDataAttribute]] = None)
    extends MarkdownLine(content, customDataAttributes)

/**
  * Definition of a link or url that can be referenced by id.
  */
case class LinkDefinition(id: String, url: String, title: Option[String])

/**
  * Stub class that is an intermediate result when parsing link definitions.
  */
case class LinkDefinitionStart(id: String, url: String) {
    def toLinkDefinition(title: Option[String]) = new LinkDefinition(id, url, title)
}

/**
  * This class allows us to reference a map with link definitions resulting from the line parsing during
  * block parsing.
  *
  * It extends a `Reader` for MarkdownLines and allows us to add the said map to the parsing context.
  * This is basically a modification of the parser monad's state.
  */
case class MarkdownLineReader private (val lines: Seq[MarkdownLine],
                                       val lookup: Map[String, LinkDefinition],
                                       val lineCount: Int)
        extends Reader[MarkdownLine] {
    /**
      * Not existing line that signals EOF.
      * This object cannot be referenced by any other code so it will fail all line parsers.
      */
    private object EofLine extends MarkdownLine("\nEOF\n")

    def this(ls: Seq[MarkdownLine], lu: Map[String, LinkDefinition]) = this(ls, lu, 1)

    def this(ls: Seq[MarkdownLine]) = this(ls, Map())

    def first = if (lines.isEmpty) EofLine else lines.head

    def rest = if (lines.isEmpty) this else new MarkdownLineReader(lines.tail, lookup, lineCount + 1)

    def atEnd = lines.isEmpty

    def pos = new Position {
        def line = lineCount
        def column = 1
        protected def lineContents = first.fullLine
    }
}