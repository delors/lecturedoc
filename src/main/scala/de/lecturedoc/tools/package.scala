/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details. 
 */
package de.lecturedoc

import java.io.{ InputStream, OutputStream, File }
import java.util.Date

import parser.Transformer

package object tools {

    private val thisClassLoader = this.getClass().getClassLoader()

    private def getHTMLFragment(baseDir: File, name: String): String = {
        Option(thisClassLoader.getResourceAsStream("Library/"+name+".fragment.html")).
            map(in ⇒ { withResource(scala.io.Source.fromInputStream(in))(_.mkString) }).
            getOrElse("<!-- No "+name+" fragment found (\"Library/"+name+".fragment.html\"). -->")
    }

    /**
      * Reads a LectureDoc document from the given `InputStream` and writes the generated HTML5 file to the
      * `OutputStream`.
      *
      * Neither the InputStream nor the OutputStream is closed.
      *
      * @param complete If true, a complete HTML document is created; otherwise a fragment is created.
      * @param lastModified The date when the underlying `.md` file was last modified.
      */
    def process(
        baseDir: File,
        in: InputStream,
        t: Transformer,
        out: OutputStream,
        complete: Boolean,
        lastModified: Date) {

        val printStream = new java.io.PrintStream(out, true, "UTF-8")
        import printStream.{ println, print }

        val input: String = scala.io.Source.fromInputStream(in)(scala.io.Codec.UTF8).mkString

        try {
            //run that string through the transformer trait's apply method
            val output = t.apply(input)

            if (complete) {
                // TODO [Refine] replace the hard coded HTML using a template language (e.g., velocity)
                // TODO [Refine] make it possible to add additional header fragments     
                // TODO [Refine] make it possible to add additional footer fragments     
                println("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name=generator content='LectureDoc'><meta name='viewport' content='width=1024, user-scalable=yes, initial-scale=1.0'>")
                println(getHTMLFragment(baseDir, "header"))
                println("</head><body data-ldjs-last-modified=\""+lastModified.getTime()+"\">")
                println("<div id='body-content'>")
                print(output)
                println("</div>")
                println(getHTMLFragment(baseDir, "footer"))
                println("</body></html>")
            }
            else {
                print(output)
            }
        }
        catch {
            case e: Exception ⇒ {
                println("Conversion failed: "+e.getLocalizedMessage())
                System.err.println("Conversion failed (see generated file for details!)")
            }
        }
    }
}